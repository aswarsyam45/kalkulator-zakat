// Penghasilan
var penghasilan = document.getElementById('peng-per');
penghasilan.addEventListener('keyup', function(e){
	penghasilan.value = formatRupiah(this.value, '');
	var peng = penghasilan.value.replace(/[.]+/g, '');

	if (peng >= 5877000) {
		var pengeluaran = parseInt($('#pengeluaran').val());
		if (pengeluaran == 0) {
			var  zakat = peng - pengeluaran;
			var pengPb = ((zakat * 2.5) / 100);
			var pengPt = pengPb * 12;
			$('#wajibjzakat').attr('value','YA');
			$('#peng-pb').attr('value', (accounting.formatNumber(pengPb, "0", ".")));
			$('#peng-pt').attr('value', (accounting.formatNumber(pengPt, "0", ".")));
		}
	} else {
		$('#wajibjzakat').attr('value','Tidak');
		$('#peng-pb').attr('value', 0);
		$('#peng-pt').attr('value', 0);
	}
var pengeluaran = document.getElementById('pengeluaran');
pengeluaran.addEventListener('keyup', function(e){
	pengeluaran.value = formatRupiah(this.value, '');
	var fix_pengeluaran = pengeluaran.value.replace(/[.]+/g, '');
	var hasil = peng - fix_pengeluaran;
		if (hasil >= 5877000) {
			var pengPb = ((hasil * 2.5) / 100);
			var pengPt = pengPb * 12;
			$('#wajibjzakat').attr('value','YA');
			$('#peng-pb').attr('value', (accounting.formatNumber(pengPb, "0", ".")));
			$('#peng-pt').attr('value', (accounting.formatNumber(pengPt, "0", ".")));
		} else {
			$('#wajibjzakat').attr('value','Tidak');
			$('#peng-pb').attr('value', 0);
			$('#peng-pt').attr('value', 0);
		}
	})
});

// Tabungan
var salBag = document.getElementById('sal-bag');
salBag.addEventListener('keyup', function () {
    salBag.value = formatRupiah(this.value, '');
    var tabungan = salBag.value.replace(/[.]+/g, '');

    if (tabungan >= 78540000) {
    	var bagHas = parseInt($('#bag-has').val());
    	if (bagHas == 0) {
    		var zakatt = tabungan + bagHas;
    		var jumlahZakat = ((zakatt * 2.5) / 100);
    		$('#wajibtzakat').attr('value', 'YA');
            $('#jumlah-zakat').attr('value', (accounting.formatNumber(jumlahZakat, "0", ".")));
    	}
    } else {
    	 $('#wajibtzakat').attr('value', 'Tidak');
    	 $('#jumlah-zakat').attr('value', 0);
    }
var bagiHasil = document.getElementById('bag-has');
bagiHasil.addEventListener('keyup', function () {
	bagiHasil.value = formatRupiah(this.value, '');
    var hasilt = bagiHasil.value.replace(/[.]+/g, '');
    console.log(hasilt);
    var fix_hasil = tabungan + hasilt;
        if (fix_hasil >= 78540000) {
            var jumlahZakat = ((fix_hasil * 2.5) / 100);
            $('#wajibtzakat').attr('value', 'YA');
            $('#jumlah-zakat').attr('value', (accounting.formatNumber(jumlahZakat, "0", ".")));
        } else {
            $('#wajibtzakat').attr('value', 'Tidak');
            $('#jumlah-zakat').attr('value', 0);
        }
    });
});

// Emas
var jumEmas = document.getElementById('jumlah-emas');
jumEmas.addEventListener('keyup', function () {
    jumEmas.value = formatRupiah(this.value, '');
    var hargae = jumEmas.value.replace(/[.]+/g, '');

    if (hargae >= 85) {
        var zakatemas = hargae * 924000;
        var jumlahZakat = (((zakatemas) * 2.5) / 100);
        $('#wajibezakat').attr('value', 'YA');
        $('#jumlah-zakat-emas').attr('value', (accounting.formatNumber(jumlahZakat, "0", ".")));
    } else {
        $('#wajibezakat').attr('value', 'Tidak');
        $('#jumlah-zakat-emas').attr('value', 0);
    }
});

// Perak
var jumPerak = document.getElementById('jumlah-perak');
jumPerak.addEventListener('keyup', function () {
    jumPerak.value = formatRupiah(this.value, '');
    var hargaa = jumPerak.value.replace(/[.]+/g, '');
    if (hargaa >= 595) {
        var zakatperak = hargaa * 8200;
        var jumlahPerak = (((zakatperak) * 2.5) / 100);
        $('#wajibperak').attr('value', 'YA');
        $('#jumlah-zakat-perak').attr('value', (accounting.formatNumber(jumlahPerak, "0", ".")));
    } else {
        $('#wajibperak').attr('value', 'Tidak');
        $('#jumlah-zakat-perak').attr('value', 0);
    }
});

// Perdagangan
$('#perdagangan').keyup(function () {
    var modPt = parseInt($('#dmodal').val());
    var keunPt = parseInt($('#dkeuntungan').val());
    var piutDg = parseInt($('#dpiutang').val());
    var hutJT = parseInt($('#dhutang').val());
    var zakat = modPt + keunPt + piutDg;
    var jumlahzakat = ((modPt + keunPt + piutDg) - (hutJT)) * (2.5 / 100);
    if (zakat >= 68850000) {
        $('#zdwajib').attr('value', 'YA');
        $('#zdagang').attr('value', (accounting.formatNumber(jumlahzakat, "0", ".")));
    } else {
        $('#zdwajib').attr('value', 'Tidak');
        $('#zdagang').attr('value', 0);
    }
});

// Pertanian
var pertanian = document.getElementById('zakat-per');
pertanian.addEventListener('keyup', function () {
    pertanian.value = formatRupiah(this.value, '');
    var harga = pertanian.value.replace(/[.]+/g, '');

    if (harga >= 5877000) {
        var zakatPer = ((harga * 5) / 100);
        $('#wajib-per').attr('value', 'YA');
        $('#jum-per').attr('value', (accounting.formatNumber(zakatPer, "0", ".")));
    } else {
        $('#wajib-per').attr('value', 'Tidak');
        $('#jum-per').attr('value', 0);
    }
});

function formatRupiah(angka, prefix) {
    var number = angka.replace(/[^\d]/g, '').toString();
    split = number.split(',');
    sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
}
