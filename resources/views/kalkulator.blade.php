<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Kalkulator Zakat</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
	<!-- Custom CSS -->
	<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
</head>
<body>
	<div class="home">
		<div class="logo">
			<a href="#"><img src="{{asset('assets/img/logo.png')}}" width="150px"></a>
		</div>
		<div class="judul">
			<p>Kalkulator Perhitungan Zakat</p>
		</div>
		<a href="" data-toggle="modal" data-target="#z-penghasilan">
			<div class="card-1">
				<i class="fas fa-wallet fa-3x" style="color: #FFA825; margin-left: 18px; margin-top: 32px;"></i>
				<i class="fas fa-wallet fa-3x"></i>
				<span class="line-1"></span>
				<p class="z-penghasilan">Zakat Penghasilan</p>
			</div>
		</a>
		<a href="" data-toggle="modal" data-target="#z-perdagangan">
			<div class="card-2">
				<i class="fas fa-shopping-bag fa-3x" style="color: #FFA825; margin-left: 18px; margin-top: 32px;"></i>
				<i class="fas fa-shopping-bag fa-3x"></i>
				<span class="line-2"></span>
				<p class="z-perdagangan">Zakat Perdagangan</p>
			</div>
		</a>
		<a href="" data-toggle="modal" data-target="#emas-perak">
			<div class="card-3">
				<i class="fas fa-ring fa-3x" style="color: #FFA825; margin-left: 18px; margin-top: 32px;"></i>
				<i class="fas fa-ring fa-3x"></i>
				<span class="line-3"></span>
				<p class="emas-perak">Emas</p>
			</div>
		</a>
		<a href="" data-toggle="modal" data-target="#z-pertanian">
			<div class="card-4">
				<i class="fas fa-balance-scale-left fa-3x" style="color: #FFA825; margin-left: 18px; margin-top: 32px;"></i>
				<i class="fas fa-balance-scale-left fa-3x"></i>
				<span class="line-4"></span>
				<p class="z-fitrah">Zakat Pertanian</p>
			</div>
		</a>
		<a href="" data-toggle="modal" data-target="#z-tabungan">
			<div class="card-5">
				<i class="fas fa-shopping-bag fa-3x" style="color: #FFA825; margin-left: 18px; margin-top: 32px;"></i>
				<i class="fas fa-shopping-bag fa-3x"></i>
				<span class="line-5"></span>
				<p class="z-tabungan">Zakat Tabungan</p>
			</div>
		</a>
		<a href="" data-toggle="modal" data-target="#perak">
			<div class="card-6">
				<i class="fas fa-ring fa-3x" style="color: #FFA825; margin-left: 18px; margin-top: 32px;"></i>
				<i class="fas fa-ring fa-3x"></i>
				<span class="line-6"></span>
				<p class="z-pertanian">Perak</p>
			</div>
		</a>
		<div class="footer">
			<p class="zakat">@ Zakat Sukses</p>
		</div>
		<div class="ellipse-1"></div>
		<div class="ellipse-2"></div>
		<div class="ellipse-3"></div>
		<div class="ellipse-4"></div>
		<div class="ellipse-5"></div>
		<div class="ellipse-6"></div>
	</div>

	{{-- Form Modal --}}
	@include('form-modal.modal')

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script src="{{asset('assets/js/accounting.js')}}"></script>
	<script src="{{asset('assets/js/hitung-zakat.js')}}"></script>
</body>
</html>